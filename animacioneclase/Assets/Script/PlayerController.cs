using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Animator playeranimator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            playeranimator.SetBool("isRunning", true);
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            playeranimator.SetBool("isJump", true);
        }
    }
}
